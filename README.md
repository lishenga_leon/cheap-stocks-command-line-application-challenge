# CheapStocksCommandLineRunnerCurrencyCheck

### About
    A commandLine Application to check the validity of a user currency based on a remote csv file
    
### Installation
    Add Java to your host machine
    Install maven to the host machine
    
### Usage/Testing
    Clone this repository from the link: https://gitlab.com/lishenga_leon/cheap-stocks-command-line-application-challenge
    Navigate to the project directory using cd command in the command line.
    Run the following command at the root of the project to install the app: mvn clean install
    Now run this command to start the application: mvn spring-boot:run

### Process Flow Theory/Design Theory
    Prompt the user to provide the currency code input
    Fetch the csv file from the server
    Look for the user input from the contents of the file
    If input is present, stop and return the findings
    Else if the user input is not present provide suggestions to the user that the user can pick from by choosing the suggestion number
    Else alert the user that the currency code is not available for usage
