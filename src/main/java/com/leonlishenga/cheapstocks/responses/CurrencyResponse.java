package com.leonlishenga.cheapstocks.responses;

import java.util.HashMap;

import com.leonlishenga.cheapstocks.models.Currency;

import lombok.Getter;
import lombok.Setter;

// Response class for Currency Model
// Uses lombok package to generate getters and setters for encapsulation
@Getter @Setter
public class CurrencyResponse<T> {

	private Integer code;

	private String message;

	private HashMap<Integer, Currency> data;
}
