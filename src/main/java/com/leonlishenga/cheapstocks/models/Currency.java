package com.leonlishenga.cheapstocks.models;

import lombok.Getter;
import lombok.Setter;

// class to represent the currency data model
// Uses lombok package to generate getters and setters for encapsulation
@Getter @Setter
public class Currency {

	// country field
	private String country;

	// currency field
	private String currency;

	// currency symbol field
	private String currencyISO;
}
