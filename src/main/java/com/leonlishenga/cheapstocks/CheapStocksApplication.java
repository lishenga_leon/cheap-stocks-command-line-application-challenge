package com.leonlishenga.cheapstocks;

import java.util.HashMap;

import com.leonlishenga.cheapstocks.service.CheapStocksService;
import com.leonlishenga.cheapstocks.responses.CurrencyResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import okhttp3.OkHttpClient;

@SpringBootApplication
public class CheapStocksApplication implements CommandLineRunner{

	// Enable the spring boot to manage dependency injection of this bean
	@Bean
	public OkHttpClient client(){
		return new OkHttpClient();
	}

	// Autowire the cheapStockServie class for the execution of the business logic
	@Autowired
	private CheapStocksService cheapStocksService;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(CheapStocksApplication.class);

		// Disable spring boot application banner
		app.setBannerMode(Mode.OFF);
		app.run(args);
	}

	// function to override the CommandLineRunner Class run function
	@Override
	public void run(String... args) {

		while (true) {

			// get the currency code input for the user
			String getUserInput = cheapStocksService.getUserInput();

			// get a status response from the business logic
			CurrencyResponse<?> currencyFileFromServer = cheapStocksService
					.getCurrencyCSVFileFromServer(getUserInput);

			// Check if the response was successful for the user inputted currency
			if (currencyFileFromServer.getCode() == 200) {

				// Check if the currency code provided was gotten from the CSV file and then alert the user
				if(currencyFileFromServer.getData().get(0) != null){
					System.out.println();
					System.out.println("Currency is Applicable for " + currencyFileFromServer.getData().get(0).getCountry());

				// Provide suggestions to the user concerning the inputted input so that the user can choose the right currency the user intended 
				}else{

					HashMap<Integer, String> didYouMeanThisCurrency = cheapStocksService.didYouMeanThisCurrencies(currencyFileFromServer.getData());

					// if the user pressed the correct key, the currency code is checked so as to notify the user
					if (didYouMeanThisCurrency.get(0).equals("YES")) {
						CurrencyResponse<?> currencyFileFromServerUserChoice = cheapStocksService.getCurrencyCSVFileFromServer(didYouMeanThisCurrency.get(1));
						System.out.println();
						System.out.println("Currency is Applicable for " + currencyFileFromServerUserChoice.getData().get(0).getCountry());
						System.out.println();
					}else{
						System.out.println();
					}

				}

			// Notify the user of any errors that occured in the business logic
			} else {
				System.out.println(currencyFileFromServer.getMessage());
			}

			// Prompt the use if he or she wants to try again
			Boolean tryAgainOneMoreTime = cheapStocksService.tryAgainOneMoreTime();

			if (!tryAgainOneMoreTime) {

				System.out.println();
				System.out.println("**********************************");
				System.out.println();
				System.out.println("Goodbye");
				System.out.println();
				System.out.println("Thank you for using Cheap Stocks");
				System.out.println();
				System.out.println("**********************************");
				System.out.println();

				return;
			}

		}

	}

}
