package com.leonlishenga.cheapstocks.service;

import com.leonlishenga.cheapstocks.models.Currency;
import com.leonlishenga.cheapstocks.responses.CurrencyResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// class with service annotation to perform business and functional logic
@Service
public class CheapStocksService {
  private Scanner scanner;

  @Autowired
  private OkHttpClient client;

  // Function for prompting the use to input the currency
  // First Display
  // Show the user a welcome message
  // Prompt the user to input the currency.
  // convert the user inputted currency to upper case
  public String getUserInput() {
    scanner = new Scanner(System.in);

    System.out.println();
    System.out.println();
    System.out.println();
    System.out.println("*** Welcome to Cheap Stocks Inc ***");
    System.out.println();
    System.out.println("**********************************");
    System.out.println();
    System.out.println(
      "Please type the currency code that you intend to use then press enter"
	);
	
    String name = scanner.nextLine();

    return name.toUpperCase();
  }

  //function for asking a user to try again one more time
  public Boolean tryAgainOneMoreTime() {
    scanner = new Scanner(System.in);

    System.out.println();
    System.out.println();
    System.out.println();
    System.out.println("**********************************");
    System.out.println();
    System.out.println("Do you want to try again ?");
    System.out.println();
    System.out.println("Press y for yes or press any other key to cancel");
    System.out.println();

	try{
		String name = scanner.nextLine();
		return name.contentEquals("y");
	}catch(InputMismatchException e){
		System.out.println();
		System.out.println("Kindly provide a string or character ?");
		System.out.println();
		
		return false;
	}
  }

  // function to fetch the currency codes csv file from the aws server
  public CurrencyResponse<?> getCurrencyCSVFileFromServer(String currencyCode) {
    try {
      Request requestHttp = new Request.Builder()
        .get()
        .url(
          "https://focusmobile-interview-materials.s3.eu-west-3.amazonaws.com/Cheap.Stocks.Internationalization.Currencies.csv"
        )
        .build();

      Call httpCall = client.newCall(requestHttp);

      // http call to get file
      Response response = httpCall.execute();

      String stringContentType = response.header("content-type");

      // check if response for the server is csv or not and check if the currency code inputted by user exists
      if (stringContentType.contentEquals("text/csv")) {
        return checkIfCurrencyIsSupported(response, currencyCode);
      } else {
        // response is not csv, most likely file was not found
        CurrencyResponse<String> currencyResponse = new CurrencyResponse<>();

        currencyResponse.setCode(400);
        currencyResponse.setMessage(
          "Oops, File containing currencies may be unavailable. Contact Admin"
        );

        return currencyResponse;
      }
    } catch (Exception e) {
      // Catch an exception if something has gone wrong
      CurrencyResponse<String> currencyResponse = new CurrencyResponse<>();

      currencyResponse.setCode(400);
      currencyResponse.setMessage(e.getMessage());

      return currencyResponse;
    }
  }

  // function for checking if currency inputted by the user is supported
  private CurrencyResponse<?> checkIfCurrencyIsSupported(
    Response response,
    String currencyCode
  ) {
    try {
      String currencyLine = "";
	  StringTokenizer stringTokenizer;

	  // Set the counter for the possible currencies to be 1
	  Integer possibleCurrenciesCount = 1;

	  // Hashmap for storing the possible currencies suggestions
	  HashMap<Integer, Currency> possibleCurrencies = new HashMap<Integer, Currency>();

      BufferedReader bufferedRead = new BufferedReader(
        new InputStreamReader(response.body().byteStream())
      );

      while ((currencyLine = bufferedRead.readLine()) != null) {
        stringTokenizer = new StringTokenizer(currencyLine, "\n");
        while (stringTokenizer.hasMoreElements()) {
          List<String> stringList = Collections
            .list(new StringTokenizer(stringTokenizer.nextToken(), ","))
            .stream()
            .map(token -> (String) token)
            .collect(Collectors.toList());

          // if currency exists, if it is supported, inform the user
          if (currencyCode.contentEquals(stringList.get(2))) {

			Currency cur = new Currency();
            cur.setCountry(stringList.get(0));
            cur.setCurrency(stringList.get(1));
            cur.setCurrencyISO(stringList.get(2));

			CurrencyResponse<Currency> currencyResponse = new CurrencyResponse<>();
			
			HashMap<Integer, Currency> currencyDetails = new HashMap<Integer, Currency>();
			currencyDetails.put(0, cur);

            currencyResponse.setCode(200);
            currencyResponse.setMessage("Currency is Supported");
            currencyResponse.setData(currencyDetails);

			return currencyResponse;
		
		  // Check if a possible currency exists with letter(s) that the user inputted
          }else if(stringList.get(2).contains(currencyCode)){
				Currency cur = new Currency();
				cur.setCountry(stringList.get(0));
				cur.setCurrency(stringList.get(1));
				cur.setCurrencyISO(stringList.get(2));

				// Store currency suggestions in a HashMap
			  	possibleCurrencies.put(possibleCurrenciesCount++, cur);
		  }
        }
	  }
	  
	  // Check if currency suggestions were gotten from the input of the user
	  if(possibleCurrencies.get(1) != null){
		CurrencyResponse<Currency> currencyResponse = new CurrencyResponse<>();
		currencyResponse.setCode(200);
		currencyResponse.setMessage("Currency is Supported");
		currencyResponse.setData(possibleCurrencies);

		return currencyResponse;
	  }

      // Currency does not exist on the file
      CurrencyResponse<String> currencyResponse = new CurrencyResponse<>();

      currencyResponse.setCode(400);
      currencyResponse.setMessage("Oops, Currency is invalid");

      return currencyResponse;
    } catch (Exception e) {
      // Catch an exception if something has gone wrong
      CurrencyResponse<String> currencyResponse = new CurrencyResponse<>();

      currencyResponse.setCode(400);
      currencyResponse.setMessage(e.getMessage());

      return currencyResponse;
    }
  }

  //function for checking if a user meant a particular currency code when the user inputted a possible wrong currency code
  public HashMap<Integer, String> didYouMeanThisCurrencies(HashMap<Integer, Currency> possibleCurrencies) {
	
	scanner = new Scanner(System.in);
	
	// check if the HashMap contains multiple values and key or just one key
	if(possibleCurrencies.get(0) == null && possibleCurrencies.size() == 1){
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("**********************************");
		System.out.println();
		System.out.println("Did you mean the following currency?");
		System.out.println();
		System.out.println("Kindly pick or choose the number");
		System.out.println();
		System.out.println("1. " + "Currency Code: " + possibleCurrencies.get(1).getCurrencyISO() + " Country: " + possibleCurrencies.get(1).getCountry());
		System.out.println();
	}else{
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("**********************************");
		System.out.println();
		System.out.println("Did you mean the following currencies?");
		System.out.println();
		System.out.println("Kindly pick or choose the number");
		System.out.println();
		for (Integer i : possibleCurrencies.keySet()) {
			System.out.println( i + ". " + "Currency Code: " + possibleCurrencies.get(i).getCurrencyISO() + " Country: " + possibleCurrencies.get(i).getCountry());
		}
		System.out.println();
	}

	HashMap<Integer, String> userChoice = new HashMap<Integer, String>();

	try{
		Integer number = scanner.nextInt();

		// Pick the currency the user choose from the suggestions
		for (Integer i : possibleCurrencies.keySet()) {
			if(number.equals(i)){
				userChoice.put(1, possibleCurrencies.get(i).getCurrencyISO());
				userChoice.put(0, "YES");
				return userChoice;
			}
		}

		// Terminate the function if the user didn't pick a suggested currency from the currency suggestions

		System.out.println();
		System.out.println("Ooops, No currency for the provided integer number found");

		userChoice.put(0, "NO");
		return userChoice;
	}catch(InputMismatchException e){
		System.out.println();
		System.out.println("Kindly provide an integer number from the suggested currencies");
		userChoice.put(0, "NO");
		return userChoice;
	}
  }
}
